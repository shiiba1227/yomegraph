"use strict";
/*==========================
ユーザーエージェント判定
==========================*/
var _ua = (function(u){
  return {
    Tablet:(u.indexOf("windows") != -1 && u.indexOf("touch") != -1 && u.indexOf("tablet pc") == -1)
    || u.indexOf("ipad") != -1
    || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
    || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
    || u.indexOf("kindle") != -1
    || u.indexOf("silk") != -1
    || u.indexOf("playbook") != -1,
    Mobile:(u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
    || u.indexOf("iphone") != -1
    || u.indexOf("ipod") != -1
    || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
    || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
    || u.indexOf("blackberry") != -1,
    ie:(u.indexOf('msie') != -1 || u.indexOf('trident') != -1),
    edge:(u.indexOf('edg') != -1 || u.indexOf('edge') == -1),
    safari:(u.indexOf('safari') != -1 && u.indexOf('edge') == -1 && u.indexOf('chrome') == -1)
  }
})(window.navigator.userAgent.toLowerCase());

if(_ua.Tablet)document.body.classList.add('ua_tab');//タブレットのとき
if(_ua.Mobile)document.body.classList.add('ua_sp');//スマホのとき
if(_ua.ie)document.body.classList.add('ua_ie');// IEのとき
if(_ua.edge)document.body.classList.add('ua_edge');// Edgeのとき
if(_ua.safari)document.body.classList.add('ua_safari');// safariのとき
