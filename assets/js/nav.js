// 変数定義 -------------------------
const memubtn = document.getElementById('memubtn');  // メニュー開閉のボタン
const navsp = document.getElementById('navsp'); // ページ全体を囲む要素 navsp
const nav = document.getElementById('nav');         // ドロワーメニュー要素 nav
const openClass = 'navOpen';                        // メニューオープン時に navsp につくclass
let navopen = false;                                // メニューオープンの判定

// 関数定義 -------------------------
const navToggle = () => {
  if (navopen) {
    navClose();
  } else {
    navOpen();
  }
};

// メニューオープン時の関数
const navOpen = () => {
  navopen = true;
  const top = window.pageYOffset;
  navsp.setAttribute('style', `top:-${top}px`);
  navsp.classList.add(openClass);
};

// メニュークローズ時の関数
const navClose  = () => {
  const closeFirst = (callback)=>{
    navopen = false;
    let scrolltop = navsp.style.top;
    scrolltop = scrolltop.slice(1,-2);
    navsp.style.top = "";
    navsp.classList.remove(openClass);
    callback(scrolltop);
  }
  closeFirst((scrolltop)=>{
    window.scrollTo(0, scrolltop);
  });
}


// 関数実行 -------------------------
if( memubtn != null){
  memubtn.addEventListener('click', navToggle, false);
}
