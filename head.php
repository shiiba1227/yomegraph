<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>
  <?php if ($title != "") {
    echo $title . " | ";
  } ?>
</title>

<link rel="shortcut icon" href="assets/images/common/favicon.ico">
<meta property="og:description" content="" />
<meta property="og:image" content="" /><!-- ogp画像は絶対パスで記述する-->
<link rel="stylesheet" href="assets/css/style.css?20210405">
<link rel="stylesheet" href="assets/plugin/swiper/swiper.css">
<link rel="stylesheet" href="assets/css/shame.css"><!-- 緊急対応用　常用しない -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
