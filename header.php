<header id="header">
  <div class="wrapper">
    <header class="head">
      <div class="logo"><a href="./"><img src="./image/common/logo.png" alt="yomegraphロゴ"></a></div>
    </header>
    <div id="navpc">
        <ul class="glnav">
          <li class="glnav-item"><a href="./">サービスの特徴</a></li>
          <li class="glnav-item"><a href="./">ご利用の流れ</a></li>
          <li class="glnav-item"><a href="./">価格表</a></li>
          <li class="glnav-item"><a href="./">お申込み</a></li>
        </ul>
    </div>
    <div id="navsp">
      <div class="memubtn" id="memubtn"></div>
      <nav class="nav" id="nav">
        <div class="nav-inner">
          <ul class="glnav">
            <li class="glnav-item"><a href="./">サービスの特徴</a></li>
            <li class="glnav-item"><a href="./">ご利用の流れ</a></li>
            <li class="glnav-item"><a href="./">価格表</a></li>
            <li class="glnav-item"><a href="./">お申込み</a></li>
          </ul>
        </div>
      </nav>
    </div>
  </div>
</header>
