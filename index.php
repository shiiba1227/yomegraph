<!DOCTYPE html>
<html lang="ja">
<head>
  <?php
  // head内の値をセット
  $title = "yomegraph";
  $keywords = "●●サイト、トップページ";
  $description = "ディスクリプションが入ります";
  ?>
  <?php include './head.php'; ?>
</head>
<body class="p-index">
  <?php include './header.php'; ?>
  <main>
    <article class="mv_slide">
      <div class="l-wrapper l_inner">
        <div class="swiper-container">
         <div class="swiper-wrapper">
           <div class="swiper-slide"><img src="assets/images/top/mv_slide1.jpg" alt=""></div>
           <div class="swiper-slide"><img src="assets/images/top/mv_slide2.jpg" alt=""></div>
         </div>
         <div class="swiper-button-prev"></div>
         <div class="swiper-button-next"></div>
      </div>
      </div>
    </article>
    <article class="mv_fukifashi">
      <div class="l-wrapper l_inner">
        <div class="mv_inner">
          <h1>
            hanayome graph<span></span>
          </h1>
          <p>
            ウエディングフォトこそ、修正技術で理想を叶えたい！<br>
            何度も見返したくなる、<br>
            綺麗な想い出を残しませんか？
          </p>
        </div>
      </div>
    </article>
    <article class="point">
      <div class="l-wrapper l-inner">
        <h2><span>POINT</span>選ばれる理由</h2>
        <div class="point_box">
          <img src="">
          <h3>圧倒的最安値</h3>
          <p>ダミーテキストテキストテキストテキストテキスト</p>
        </div>
        <div class="point_box">
          <img src="">
          <h3>圧倒的最安値</h3>
          <p>ダミーテキストテキストテキストテキストテキスト</p>
        </div>
        <div class="point_box">
          <img src="">
          <h3>圧倒的最安値</h3>
          <p>ダミーテキストテキストテキストテキストテキスト</p>
        </div>
      </div>
    </article>

    <article class="contents">
      <div class="l-wrapper l-inner">
        <section>あああ</section>
        <section></section>

      </div>
    </article>
  </main>


  <?php include './footer.php'; ?>
  <script src="assets/plugin/swiper/swiper.min.js"></script>
  <script>
  var swiper = new Swiper('.swiper-container', {
  　loop: true,
  autoplay: {
    delay: 5000,
  },
  centeredSlides:true,
  slidesPerView:2,
  });
  </script>
</body>
</html>
